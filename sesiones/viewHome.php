<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
</head>
<body>
    <h3>Agregar deseo</h3>
    <form method="post" action="?method=new">
        <label>Nuevo deseo: </label>
        <input type="text" name="deseo">
        <input type="submit" name="Agregar"><br>
    </form>
    <hr>
    <h1>Lista de deseos de <?php echo isset($_SESSION["usuario"]) ? $_SESSION["usuario"] : "" ?></h1>
    <ul>
        <?php foreach ($deseos as $clave => $deseo): ?>
            <li><?php echo $deseo ?>
                <a href="?method=borrar&clave=<?php echo $clave ?>&deseo=<?php echo $deseo ?>">Borrar</a>
            </li>
        <?php endforeach ?>
    </ul>
    <a href="?method=close">Cerrar sesion</a>
</body>
</html>
