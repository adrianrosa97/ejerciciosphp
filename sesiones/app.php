<?php

/**
*
*/
class App
{

    function __construct()
    {
        session_start();
    }

    public function login(){


        require("login.php");


        //echo "Login!!<br>";
    }
    public function auth(){
        if (isset($_REQUEST["usuario"]) && !empty($_REQUEST["usuario"]) &&
            isset($_REQUEST["contra"]) && !empty($_REQUEST["contra"])){

            $usuario = $_REQUEST["usuario"];
            $contraseña = $_REQUEST["contra"];

            $_SESSION["usuario"] = $usuario;
            $_SESSION["contra"] = $contraseña;

            header("Location:index.php?method=home");
        }else{
            header("Location:index.php?method=login");
        }

        //$this->home();
    }
    public function home(){

        if(!isset($_SESSION["usuario"])){
            header("Location:index.php?method=login");
            return;
        }

        $user = $_SESSION["usuario"];
        if(isset($_SESSION["deseos"])){
            $deseos = $_SESSION["deseos"];
        }else{
            $deseos = [];
        }
    require("viewHome.php");

    }

    public function new(){
        if(isset($_REQUEST["deseo"]) &&
            !empty($_REQUEST["deseo"])){
            $_SESSION["deseos"][] = $_REQUEST["deseo"];

            // $deseos = $_SESSION["deseos"];
            // $deseos[] = $_REQUEST["deseo"];
            // $_SESSION["deseos"] = $deseos;
        }
        header("Location:?method=home");
    }

    public function close(){
        echo "Sesion cerrada!!";
        session_destroy();
        unset($_SESSION);
        header("Location:?method=login");
    }

    public function borrar(){
        echo "$_REQUEST[deseo] borrado!!";
        $key = (integer)$_REQUEST["clave"];
        unset($_SESSION["deseos"][$key]);
        header("Location:?method=home");
    }
}
?>
