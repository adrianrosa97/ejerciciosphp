<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios php</title>
</head>
<body>
<h1>Ejercicios PHP</h1>
<h2>Alumno: Adrian Rosa Camin</h2>
Crea enlaces a tus ejercicios:
<ul>
    <li><a href="/ejercicio1.php"> Ejercicio 1</a></li>
    <li><a href="/ejercicio2.php"> Ejercicio 2</a></li>
    <li><a href="/ejercicio3.php"> Ejercicio 3</a></li>
    <li><a href="/ejercicio4.php"> Ejercicio 4</a></li>
    <li><a href="/ejercicio5.html"> Ejercicio 5</a></li>
    <li><a href="/ejercicio6.php"> Ejercicio 6</a></li>
    <li><a href="/ejercicio7.php"> Ejercicio 7</a></li>
    <li><a href="/ejercicio8.php"> Ejercicio 8</a></li>
    <li><a href="/cookies.php"> Ejercicio Cookies</a></li>
    <li><a href="/EjerciciosTema2/Calculadora"> Calculadora</a></li>
    <li><a href="/EjerciciosTema2/Galeria"> Galeria</a></li>
    <li><a href="/EjerciciosTema2/Loteria"> Loteria</a></li>
</ul>
</body>
</html>
