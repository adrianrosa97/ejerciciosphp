<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 3</title>
</head>
<body>
    <h1>Dias de la semana en un Array</h1>
    <?php
        $diasSemana = array("Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo");
        echo "<ul>";
        foreach ($diasSemana as $posicion => $elemento) {
            echo "<li><B>$elemento</B>.</li>";
        }
        echo "</ul>";
        echo "<table border=1><br><tr>";
        foreach ($diasSemana as $posicion => $elemento) {
            echo "<td><B>$elemento</B></td>";
        }
        echo "</tr><br></table>";
     ?>
    <table border=1>
        <?php foreach ($diasSemana as $orden => $dia): ?>
            <tr>
                <td><?php echo $orden + 1?></td>
                <td><?php echo $dia?></td>
            </tr>
        <?php endforeach?>
    </table>
</body>
</html>
