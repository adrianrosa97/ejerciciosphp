<!DOCTYPE html>
<html>
<head>
    <title>Loteria</title>
    <link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
    <h1 align="center">Loteria</h1>
    <form action="?method=realizar" method="post">
    <table class="tableClass" border="1" align="center">
        <?php for ($i=1; $i <= 49; $i++) {
            echo "<td><a href='?method=apostar&Numero=$i'>$i</a></td>";
            if ($i % 7 == 0) {
                echo "</tr><tr>";
            }

        } ?>
    </table>
    <h3 align="center"><?php echo !empty($mensaje) ? $mensaje : "" ?></h3>
    <div align="center" style="background-color: green">
        <input type="submit" name="Enviar" value="Apostar">
    </div>

    </form>
    <hr>
    <div class="apuestas" align="center">
        <?php if (isset($_SESSION['apuesta']) && !empty($_SESSION['apuesta'])): ?>
            <?php foreach ($_SESSION['apuesta'] as $key => $value): ?>
                Has apostado al numero: <?php echo $value ?><br>
            <?php endforeach ?>
        <?php endif ?>
    </div>
</body>
</html>
