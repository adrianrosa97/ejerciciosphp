<?php
/**
*   Clase que actua de controlador para la calculadora.
*/
class App
{
    //Array para guardar los numeros apostados.
    var $apuesta = [];
    //Mensaje de error.
    var $mensaje = "";
    function __construct()
    {
        session_start();
    }
    public function loteria(){
        require "loteria.php";
    }
    public function apostar(){
        if(isset($_SESSION['apuesta']) && !empty($_SESSION['apuesta'])){
            $apuesta = $_SESSION['apuesta'];
        }

        if(!empty($apuesta[$_REQUEST['Numero']])){
            unset($apuesta[$_REQUEST['Numero']]);
        }else{
            $apuesta[$_REQUEST['Numero']] = $_REQUEST['Numero'];
        }

        $_SESSION['apuesta'] = $apuesta;

        require "loteria.php";
    }
    public function realizar(){
        $apuesta = $_SESSION['apuesta'];
        if(isset($apuesta)){
            if(count($apuesta) < 6){
                $mensaje = 'La apuesta minima es de 6 y tu tienes ' . count($apuesta);
            }else if (count($apuesta) == 6) {
                $mensaje = "Apuesta simple";
            }else{
                $mensaje = "Apuesta multiple: " . count($apuesta);
            }
        }
        require "loteria.php";
    }
}
 ?>
