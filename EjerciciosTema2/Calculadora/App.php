<?php
/**
*   Clase que actua de controlador para la calculadora.
*/
class App
{
    function __construct()
    {
        //Iniciamos la sesion
        session_start();
    }

    public function operacion(){
        //Abrimos la pagina para operar
        require("operacion.php");
    }

    public function resultado(){
        //Borramos el arror antiguo
        if(isset($_SESSION['error']))
            unset($_SESSION['error']);
        unset($_SESSION['solucion']);
        //Comprobamos si se han introducido los numeros
        if(isset($_REQUEST["numero1"]) && is_numeric($_REQUEST["numero1"]) &&
            isset($_REQUEST["numero2"]) && is_numeric($_REQUEST["numero2"])){


            //setcookie("numero1","",time() - 3600);
            //setcookie("numero2","",time() - 3600);
            //setcookie("res","",time() - 3600);
            //Comprobamos de que sean numeros
            // if(is_numeric($_REQUEST["numero1"]) && is_numeric($_REQUEST["numero2"]))
            // {
                //Guardamos en variables los numeros
                $numero1 = $_REQUEST["numero1"];
                $numero2 = $_REQUEST["numero2"];
                $res = 0;

                //Depende del valor  del select, hara una operacion u otra
                if($_REQUEST["operacion"] == "suma"){
                    $res = $numero1 + $numero2;
                }
                elseif ($_REQUEST["operacion"] == "resta") {
                    $res = $numero1 - $numero2;
                }
                elseif ($_REQUEST["operacion"] == "multiplicacion") {
                    $res = $numero1 * $numero2;
                }
                elseif ($_REQUEST["operacion"] == "division") {
                    $res = $numero1 / $numero2;
                }

                //Guardamos los datos en la sesion
                // $_SESSION["numero1"] = $numero1;
                // $_SESSION["numero2"] = $numero2;
                // $_SESSION["res"] = $res;
                $_SESSION["operacion"] = "La operacion de $_REQUEST[operacion] entre los numeros $numero1 y $numero2 es: $res";
                // setcookie("numero1",$numero1);
                // setcookie("numero2",$numero2);
                // setcookie("res",$res);

                //Redireccionamos a la pagina de la operacion con los resultados
                header("Location:index.php?method=operacion");
            // }else{
            //     $_SESSION["error"] = "Los datos introducidos no son numeros.";
            //     //echo "<h1>Los datos introducidos no son numeros.</h1>";
            // }
        }else{
            $_SESSION["error"] = "Debes rellenar todos los datos.";
                header("Location:index.php?method=operacion");
            //echo "<h1>Debes rellenar todos los datos.</h1>";
        }

    }
}
 ?>
