<!DOCTYPE html>
<html>
<head>
    <title>Calculadora</title>
</head>
<body>
    <h1>Calculadora en PHP</h1><br>
    <!--Parrafo para imprimir si tenemos algun mensaje de error-->
    <p style="color: red"><?php echo isset($_SESSION['error']) ? $_SESSION['error'] : '' ?></p>
    <!--Formulario-->
    <form method="post" action="?method=resultado">
        <label>Numero 1: </label>
        <input type="number" step="any" name="numero1"><br>
        <label>Operacion:  </label>
        <select name="operacion">
            <option value="suma">Suma</option>
            <option value="resta">Resta</option>
            <option value="multiplicacion">Multiplicacion</option>
            <option value="division">Division</option>
        </select><br>
        <label>Numero 2: </label>
        <input type="number" step=".01" name="numero2"><br>
        <hr>
        <label>Resultado: </label>
        <!--Label para imprimir el resultado de la operacion-->
        <label><?php echo isset($_SESSION["operacion"]) ? $_SESSION["operacion"] : 'Sin resultado' ?>
        </label>
        <br>
        <br>
        <br>
        <input type="submit" name="Realizar">
    </form>
</body>
</html>
