
<?php
//Indicamos el archivo php a abrir.
require "App.php";

//Creamos una instancia de App.
$app = new App();

//Comprobamos si hemos recibido algun metodo por parametro, si lo hemos recibido lo redireccionamos, si hemos recibido uno que no existe nos redireccionara a uno por defecto y si no recibimos ninguno tambien al de por defecto.
if(isset($_GET["method"])){
    $method = $_GET["method"];
}else{
    $method = "operacion";
}
if(!method_exists("app",$method)){
    $method = "operacion";
}
//Redireccionamos.
$app->$method();
 ?>

