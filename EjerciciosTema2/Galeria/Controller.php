<?php

/**
*   Clase que actua de controlador para la galeria.
*/
class Controller
{


    function __construct()
    {
    }

    //Funcion por defecto.
    function galeria(){
        require "viewGaleria.php";
    }
    //Funcion para subir la imagen.
    function subirImagen(){
        //Variable para guardar el error;
        $mensaje = "";
        if(isset($_FILES["ficheroSubido"]) && !empty($_FILES["ficheroSubido"])){
            $uploadedfile_size = $_FILES['ficheroSubido']['size'];
            $subido = true;
            if ($_FILES['ficheroSubido']['size'] > 200000) {
                $mensaje = "El archivo es mayor que 200KB, debes reduzcirlo antes de subirlo<br>";
                $subido = false;
            }

            if (!($_FILES['ficheroSubido']['type'] =="image/jpeg" || $_FILES['ficheroSubido']['type'] =="image/gif")) {
                $mensaje = " Tu archivo tiene que ser JPG o GIF.<br>";
                $subido = false;
            }
            $file_name = $_FILES['ficheroSubido']['name'];
            $add="imagenes/$file_name";
            if ($subido) {
                if (move_uploaded_file($_FILES['ficheroSubido']['tmp_name'], $add)) {
                    $mensaje = " Ha sido subido satisfactoriamente";
                } else {
                    $mensaje = "Error al subir el archivo";
                }
            }
        }else{
            $mensaje = "No se ha seleccionado ninguna imagen.";
        }
        require "viewGaleria.php";
    }
    function eliminarImagen(){
        $imagen = $_REQUEST["file"];
        unlink($imagen);
        $mensaje = "Imagen borada";
        require "viewGaleria.php";
    }
}
?>
