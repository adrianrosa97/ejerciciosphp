<?php
//Indicamos el archivo php a abrir.
require "Controller.php";

//Creamos una instancia de App.
$app = new Controller();

//Comprobamos si hemos recibido algun metodo por parametro, si lo hemos recibido lo redireccionamos, si hemos recibido uno que no existe nos redireccionara a uno por defecto y si no recibimos ninguno tambien al de por defecto.
if(isset($_GET["method"])){
    $method = $_GET["method"];
}else{
    $method = "galeria";
}
if(!method_exists("Controller", $method)){
    $method = "galeria";
}
//Redireccionamos.
$app->$method();

