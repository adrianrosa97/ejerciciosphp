<!DOCTYPE html>
<html>
<head>
    <title>Galeria</title>
</head>
<body>
    <h1>Galeria</h1>
    <form enctype="multipart/form-data" action="?method=subirImagen" method="post">
        <label style="color: red"><?php echo !empty($mensaje) ? $mensaje : "" ?></label><br>
        <input name="ficheroSubido" type="file" />
        <input type="submit" value="Subir imagen" />
    </form><hr>
    <?php
        //nombre de la carpeta
        $dir='imagenes/';
        //Recogemos del directorio solo las imagenes con las extensiones
        //.gif y .jpg.
        $images = glob("$dir{*.gif,*.jpg,}", GLOB_BRACE);
        //Recorremos e impimimos las imagenes.
        foreach($images as $v){
            if($v != $dir){
                echo '<br><a href="?method=eliminarImagen&file='.$v.'">Eliminar</a>';
                echo '<img src="'.$v.'" border="0" style="width:100px;" />';

            }
        }
    ?>
    <!-- <?php if (!empty($listaImagenes)): ?>
        <?php foreach ($listaImagenes as $key => $imagen): ?>
            <img src="<?php echo $imagen ?>" width="30px"><br>
        <?php endforeach?>
    <?php endif ?> -->
</body>
</html>
