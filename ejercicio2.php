<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 2</title>
</head>
<body>
    <h1>IVA de un producto</h1>
    <?php
        $nombreProducto = "Pelota";
        $cantidad = 1;
        $precio = 5;
        $iva = 21;
        $precioConIva = $precio * 1.21;
        $precioSinIva = $precio;

        echo "El producto es: <B>$nombreProducto</B>. <br>La cantidad seleccionada es: <B>$cantidad</B>. <br>El precio del producto por unidad <B>sin iva</B> es: <B>$precioSinIva</B> euros sin iva. <br>El precio del producto por unidad <B>con iva</B> es: <B>$precioConIva</B> euros. <br>El IVA es de:<B>" .$iva. "%</B>.";
     ?>

</body>
</html>
