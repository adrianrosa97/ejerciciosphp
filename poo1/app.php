<?php

/**
* Clase principal de mi aplicacion.
*/
class App
{
    private $atributo;
    public $name = "";
    public $surname = "";
    function __construct($param)
    {
        // echo "Construyendo mi app<br>";
        $this->atributo = $param;
        //$this->index();
    }
    public function index(){
        // echo "En mi metodo index<br>";
        require("viewLogin.php");
    }

    public function hello(){
        echo "hello world";
    }

    public function home()
    {
        if (isset($_REQUEST["txtNombre"]) && !empty($_REQUEST["txtNombre"]) &&
    isset($_REQUEST["txtApellidos"]) && !empty($_REQUEST["txtApellidos"])){
            if(isset($_REQUEST["recordar"])){
                setcookie("recordar",$_REQUEST["recordar"]);
            }else{
                setcookie("name", "", time() - 3600);
                setcookie("surname", "", time() - 3600);
                setcookie("recordar", "", time() - 3600);
            }
            $name = $_REQUEST["txtNombre"];
            $surname = $_REQUEST["txtApellidos"];
            setcookie("name",$name);
            setcookie("surname",$surname);
        }
        require("viewHome.php");
    }
}
