<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 4</title>
</head>
<body>
    <h1>Dias de la semana en un Array</h1>
    <?php
        $equipo = [
            1 => "Adrian",
            2 => "Alex",
            3 => "Hector",
            4 => "Mario",
            5 => "Andres"
        ];
        echo "<ul>";
        foreach ($equipo as $posicion => $persona) {
            echo "<li><B>$persona</B>.</li>";
        }
        echo "</ul>";
        echo "<ul>";
        foreach ($equipo as $posicion => $persona) {
            echo "<li><B>" .$persona[$posicion]."</B>.</li>";
        }
        echo "</ul>";

    ?>
</body>
</html>
